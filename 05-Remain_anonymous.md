# Section-5 Remain anonymous on the web

**As an hacker, we always want to be anonymous...**
<br>

> What is privacy?

Privacy can be defined as the state or condition of being free from being observed or disturbed by other people.
<br><br>

> Why it's important to remain private?

As an ethical hacker we want to mimic what an actual black hat hacker will do, so in order to do it we need to be anonymous over the web.
<br><br>

> Finding your IP address

Goto google and type **what is my ip**. It will so you your IP address and your location.
<br><br>

> VPN sample
```
                      _______________________VPN TUNNEL______________________
                     |------> ------> ------> ------> ------> ------> ------>| 
             [SOURCE]                         |DATA|                          [DESTINATION]   
                     |------> ------> ------> ------> ------> ------> ------>|
                     |_______________________VPN TUNNEL______________________|
```

It will hide our IP and encrypt our data over the internet so we will achive anonymity.
<br><br>

> TOR browser

- The TOR browser is a web browser that allows users to browse the web while preventing surveillance and tracking.
- TOR routs your requests through 3 other different nodes(computers).
<br>

**Installing TOR in kali linux**
```md
1. **Update the repositories**
vi /etc/apt/sources.list

2. **Add this line in it**
deb http://http.kali.org/kali kali-rolling main contrib non-free

3. **Update changes**
sudo apt-get update

4. **Update Keyserver(optional)**
apt-key adv --keyserver hkp://keys.gnupg.net --recv-keys

5. **Downloading TOR**
sudo apt-get install tor torbrowser-launcher

6. **Installing TOR launcher**
torbrowser-launcher
```
<br>

> Anonsurf

It is a tool that will help you to stay anonymous by routing every packet from your computer through TOR relay.

**Install Anonsurf**
```sh
1. git clone https://github.com/Und3rfl0w/kali-anonsurf.git
2. cd kali-anonsurf
3. ./installer.sh
4. anonsurf start
```
<br>

> Change MAC address

Every device has a unique MAC address that can identify it. In roder to remain anonymous, it would be wise to change this as well.

**Install MACchanger**
```sh
sudo apt install macchanger 
sudo macchanger -r (interface name)
```
<br>

> Using a VPN

It is a process to secure our network, we use VPN tunnel to hide our IP and encrypt our data over the internet so we will achive anonymity.